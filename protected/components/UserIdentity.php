<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
  private $_id;

  /**
   * Authenticates a user.
   * The example implementation makes sure if the username and password
   * are both 'demo'.
   * In practical applications, this should be changed to authenticate
   * against some persistent user identity storage (e.g. database).
   * @return boolean whether authentication succeeds.
   */
  public function authenticate()
  {
    $nick = strtolower($this -> username);
    $user = User::model() -> find('active=1 AND LOWER(nick)=?', array($nick));
    if ($user === null || $this -> password !== $user -> pass)
      $this -> errorCode = self::ERROR_USERNAME_INVALID;
    //else if($users[$this->username]!==$this->password)
    //  $this->errorCode=self::ERROR_PASSWORD_INVALID;
    else {
      $this -> _id = $user -> id;
      $this -> username = $user -> nick;
      $this -> errorCode = self::ERROR_NONE;
    }
    return !$this -> errorCode;
  }

  public function getId () {
    return $this -> _id;
  }
}
