<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
  'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
  'name'=>'pastii',
  'defaultController' => 'paste',

  // preloading 'log' component
  'preload'=>array('log', 'bootstrap'),

  // autoloading model and component classes
  'import'=>array(
    'application.models.*',
    'application.components.*',
  ),

  // application components
  'components'=>array(
    // Uncomment 'request' array to add CSRF validation
    /*
    'request'=>array(
      'enableCsrfValidation'=>true,
    ),
    */

    /*
    'log'=>array(
      'class'=>'CLogRouter',
      'routes'=>array(
        array(
          'class'=>'CProfileLogRoute',
        ),
        array(
          'class'=>'CFileLogRoute',
        ),
        array(
          'class'=>'CWebLogRoute',
        ),
      ),
    ),
    */

    'user'=>array(
      // enable cookie-based authentication
      'allowAutoLogin'=>true,
    ),
    // uncomment the following to set up database
    'db'=>array(
      'class'=>'CDbConnection',
      'connectionString'=>'sqlite:' . dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR  . 'data' . DIRECTORY_SEPARATOR . 'pastii.db',
      'enableParamLogging' => true,
      'enableProfiling' => true,
    ),
    'urlManager'=>array(
      'urlFormat'=>'path',
      'showScriptName' => false,
      'rules' => array(
        '' => 'paste/create',
        'c' => 'paste/create',
        'c/<nick:.+>' => 'paste/create',
        '<id:\d+>' => 'paste/show',
        '<id:\d+>/<p:.+>' => 'paste/show',
      ),
      //'urlSuffix'=>'.html',
    ),
    'bootstrap'=>array(
      'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
      'coreCss'=>true, // whether to register the Bootstrap core CSS (bootstrap.min.css), defaults to true
      'responsiveCss'=>false, // whether to register the Bootstrap responsive CSS (bootstrap-responsive.min.css), default to false
      'plugins'=>array(
        // Optionally you can configure the "global" plugins (button, popover, tooltip and transition)
        // To prevent a plugin from being loaded set it to false as demonstrated below
        'transition'=>false, // disable CSS transitions
        'tooltip'=>array(
          'selector'=>'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
          'options'=>array(
            'placement'=>'bottom', // place the tooltips below instead
          ),
        ),
        // .....
        // If you need help with configuring the plugins, please refer to Bootstrap's own documentation:
        // http://twitter.github.com/bootstrap/javascript.html
      ),
    ),
    'lessCompiler'=>array(
      'class'=>'ext.less.components.LessCompiler',
      'paths'=>array(
        'less/style.less'=>'css/style.css',
      ),
    ),
  ),

  // application-level parameters that can be accessed
  // using Yii::app()->params['paramName']
  'params'=>array(
    'admin' => array('admin'),
    'salt' => '5d77d98414cc35f647cd92cf0e78af14',
    'pastesOnHome' => 5,
    // Uncomment next line to enable Akismet
    // 'akismetKey' => 'YOUR_CODE',
  ),

  'behaviors'=>array(
    'ext.less.components.LessCompilationBehavior',
  ),
);
