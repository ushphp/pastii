<?php
Yii::import('application.vendors.*');
// socket akismet library:
// require_once('Akismet/Akismet.class.php');
// curl akismet library:
require_once('Akismet/akismet.curl.class.php');

class PasteController extends CController
{
  const PAGE_SIZE = 20;

  /**
   * @var string specifies the default action to be 'list'.
   */
  public $defaultAction='create';

  /**
   * @var CActiveRecord the currently loaded data model instance.
   */
  private $_model;

  /**
   * @return array action filters
   */
  public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
      array('allow',  // allow all users to perform 'list' and 'show' actions
        'actions'=>array('list', 'search', 'show','create','captcha','raw', 'reply', 'prune', 'json', 'ajax', 'feed'),
        'users'=>array('*'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions'=>array('admin','delete','update'),
        'users'=>array('@'),
      ),
      array('deny',  // deny all users
        'users'=>array('*'),
      ),
    );
  }

  /**
   * Shows a particular model.
   */
  public function actionShow()
  {
    $_mod = Paste::model();
    $model = $this->loadPaste();
    $pasteId = $model->pasteId;

    if ($pasteId)
      $parent = $_mod -> findbyPk($pasteId);
    else
      $parent = false;

    $childs = $_mod -> findAllByAttributes(array('pasteId'=>$model->id));

    $this->render('show',array('model'=>$model, 'parent' => $parent, 'childs' => $childs));
  }

  public function actionRaw()
  {
    $this -> layout = false;
    header('Content-Type: text/plain');
    $this -> render('raw', array('model' => $this -> loadPaste()));
  }

  public function actionReply() {
    $nick = '';
    if (isset($_GET['nick']))
      $nick = CHtml::encode($_GET['nick']);
    if(isset($_POST['Paste'])) {
      if ($this->akismetApproved($_POST['Paste'])) {
        $model = new Paste;
        $model -> setScenario('reply');
        $model -> attributes = $_POST['Paste'];
        if($model -> save()) {
          $url = $model -> id;
          if (isset($model -> privateKey) && $model -> privateKey != '')
            $url .= '/' . $model -> privateKey;
          if (isset($_SERVER['PHP_AUTH_USER'])) {
            echo $url;
            exit(1);
          }
          $this -> redirect(array('/' . $url));
        }
        $nick = CHtml::encode($_POST['Paste']['nick']);
      }
      else {
        echo 'Spam detected...';
        exit(1);
      }
    }
    $model = $this -> loadPaste();
    $models = $model -> pastesOnHome(Yii::app() -> params['pastesOnHome']);
    $this -> render('reply', array('model' => $model, 'nick' => $nick, 'models' => $models));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'show' page.
   */
  public function actionCreate()
  {
    $model = new Paste;
    $model -> setScenario('create');
    $nick = '';
    if (isset($_GET['nick']))
      $nick = CHtml::encode($_GET['nick']);
    if(isset($_POST['Paste']))
    {
      if ($this->akismetApproved($_POST['Paste'])) {
        $model -> attributes = $_POST['Paste'];
        if($model -> save()) {
          $url = $model -> id;
          if (isset($model -> privateKey) && $model -> privateKey != '')
            $url .= '/' . $model -> privateKey;
          if (isset($_POST['cli'])) {
            echo $url;
            exit(1);
          }
          else
            $this -> redirect(array('/' . $url));
        }
        $nick = CHtml::encode($_POST['Paste']['nick']);
      }
      else {
        echo 'Spam detected...';
        exit(1);
      }
    }
    $models = $model -> pastesOnHome(Yii::app() -> params['pastesOnHome']);
    $this -> render('create',array('model' => $model, 'nick' => $nick, 'models' => $models));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'show' page.
   */
  public function actionUpdate()
  {
    $isAdmin = in_array(Yii::app() -> user -> name, Yii::app() -> params -> admin);
    $model = $this -> loadPaste();

    if(isset($_POST['Paste']))
    {
      $model->attributes=$_POST['Paste'];
      if($model->save())
        $this->redirect(array('show','id'=>$model->id));
    }
    $this->render('update',array('model'=>$model));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'list' page.
   */
  public function actionDelete()
  {
    if(Yii::app()->request->isPostRequest)
    {
      // we only allow deletion via POST request
      $this->loadPaste()->delete();
      $this->redirect(array('list'));
    }
    else
      throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Lists all models.
   */
  public function actionList()
  {
    $criteria = new CDbCriteria;
    $criteria -> order = 'createTime DESC';
    $criteria -> condition = '(privateKey = \'\' OR privateKey IS NULL)';

    if (isset($_GET['type'])) {
      $criteria -> condition .= ' AND type=:type';
      $criteria -> params[':type'] = $_GET['type'];
    }
    if (isset($_GET['author'])) {
      $criteria -> condition .= ' AND nick=:author';
      $criteria -> params[':author'] = $_GET['author'];
    }
    elseif (isset($_GET['search']) && $_GET['search'] != '') {
      $search = '%' . $_GET['search'] . '%';
      $criteria -> condition .= ' AND text LIKE :search OR title LIKE :search';
      $criteria -> params[':search'] =  $search;
    }
    $pages = new CPagination(Paste::model() -> count($criteria));

    $pages -> pageSize = isset($_GET['limit'])? $_GET['limit']: self::PAGE_SIZE;
    $pages -> applyLimit($criteria);

    $models = Paste::model() -> findAll($criteria);

    $this -> render($this -> action -> id, array(
      'models' => $models,
      'pages' => $pages,
    ));
  }

  public function actionSearch() {
    $this -> redirect(array('list','search' => $_GET['search']));
  }

  public function actionAjax() {
    $this -> layout = false;
    $this -> actionList();
  }

  public function actionJson() {
    $this -> layout = false;
    $this -> actionList();
  }

  public function actionFeed() {
    Yii::import('application.extensions.webfeed.EWebFeed');
    header('Content-Type: application/xml charset=UTF-8');
    $this -> layout = false;
    $this -> actionList();
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    if (isset($_POST['2del']) && isset($_POST['2del'][0]))
      Paste::model() -> deleteAll('id IN (' . implode(',', $_POST['2del']) . ')');
    $this->processAdminCommand();

    $criteria = new CDbCriteria;
    $condition = array();
    if (isset($_GET['user']) && $_GET['user'] != '')
      $condition[] = 'Paste.nick = ' . (int)$_GET['user'];
    if (isset($_GET['ip'])   && $_GET['ip'] != '')
      $condition[] = 'Paste.ip = \''     . $_GET['ip'] . '\'';
    if (isset($condition[0]))
      $criteria -> condition = implode(' AND ', $condition);

    $criteria -> order = 'Paste.createTime DESC';

    $pages=new CPagination(Paste::model()->count($criteria));
    $pageSize = (int)(isset($_GET['pageSize'])? $_GET['pageSize']: self::PAGE_SIZE);
    if ($pageSize > 0) {
      $pages->pageSize = $pageSize;
      $pages->applyLimit($criteria);
    }

    $sort=new CSort('Paste');
    $sort->applyOrder($criteria);

    $models=Paste::model()->findAll($criteria);

    $this->render('admin',array(
      'models'=>$models,
      'pages'=>$pages,
      'sort'=>$sort,
    ));
  }

  public function actionPrune() {
    $condition = 'until IS NOT NULL AND until != 0 AND (updateTime + until) < ' . time();
    $number = Paste::model() -> deleteAll($condition);
    echo $number;
    exit(1);
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
   */
  public function loadPaste($id=null)
  {
    $isAdmin = in_array(Yii::app() -> user -> name, Yii::app() -> params -> admin);
    if($this->_model===null)
    {
      if($id!==null || isset($_GET['id']))
        $this->_model=Paste::model() -> findbyPk($id!==null ? $id : $_GET['id']);
      $auth = isset($_GET['p']) ? $_GET['p']: '';
      if($this->_model===null || !$isAdmin && $auth != $this -> _model -> privateKey)
        throw new CHttpException(404,'The requested page does not exist.');
    }
    return $this->_model;
  }

  /**
   * Executes any command triggered on the admin page.
   */
  protected function processAdminCommand()
  {
    if(isset($_POST['command'], $_POST['id']) && $_POST['command']==='delete')
    {
      $this->loadPaste($_POST['id'])->delete();
      // reload the current page to avoid duplicated delete actions
      $this->refresh();
    }
  }

  /**
   * Returns true if is aproved or akismet is disabled, or if akismet server is unavailable.
   */
  /*
  // no curl (socket) based library spam detect function
  private function akismetApproved($model) {
    $akismetKey = Yii::app() -> params['akismetKey'];
    if ($akismetKey) {
      $site = $this->createAbsoluteUrl('/');
      try {
        $akismet = new Akismet($site, $akismetKey);
        if (isset($model['nick']) && $model['nick'])
          $akismet->setCommentAuthor($model['nick']);
        $akismet->setCommentContent($model['text']);
        return !$akismet->isCommentSpam();
      }
      catch (Exception $e) {
        return true;
      }
    }
    else
      return true;
  }
  */

  // curl based library spam detect function
  private function akismetApproved($model) {
    $akismetKey = Yii::app() -> params['akismetKey'];
    if ($akismetKey) {
      $site = $this->createAbsoluteUrl('/');
      try {
        $akismet = new akismet($akismetKey, $site);
        $comment = array('comment_content'=>$model['text']);
        if (isset($model['nick']) && $model['nick'])
          $comment['comment_author'] = $model['nick'];
        return !$akismet->is_spam($comment);
      }
      catch (Exception $e) {
        return true;
      }
    }
    else
      return true;
  }
}
