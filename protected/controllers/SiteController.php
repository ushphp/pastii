<?php

class SiteController extends CController
{
  /**
   * Declares class-based actions.
   */
  public function actions()
  {
    return array(
      // captcha action renders the CAPTCHA image
      'captcha'=>array(
        'class'=>'CCaptchaAction',
        'backColor'=>0,
        'foreColor'=>0x00ddff,
        'minLength' => 4,
        'transparent' => true
      ),
    );
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex()
  {
    // renders the view file 'protected/views/site/index.php'
    // using the default layout 'protected/views/layouts/main.php'
    $this->render('index');
  }

  /**
   * Displays the contact page
   */
  /*
  public function actionContact()
  {
    $contact=new ContactForm;
    if(isset($_POST['ContactForm']))
    {
      $contact->attributes=$_POST['ContactForm'];
      if($contact->validate())
      {
        $headers="From: {$contact->email}\r\nReply-To: {$contact->email}";
        mail(Yii::app()->params['adminEmail'],$contact->subject,$contact->body,$headers);
        Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
        $this->refresh();
      }
    }
    $this->render('contact',array('contact'=>$contact));
  }
  */

  /**
   * Displays the login page
   */
  public function actionLogin()
  {
    $form = new LoginForm;
    // collect user input data
    if(isset($_POST['LoginForm']))
    {
      $form -> attributes=$_POST['LoginForm'];
      // validate user input and redirect to previous page if valid
      $url = Yii::app() -> user -> returnUrl;
      // un bugcito, a veces la url viene cerrando con index.php, si es así lo cortamos :P
      if (strpos(strrev($url), 'php.xedni') === 0)
        $url = dirname($url);
      if($form -> validate())
        $this -> redirect($url);
    }
    // display the login form
    $this -> render('login',array('form' => $form));
  }

  /**
   * Logout the current user and redirect to homepage.
   */
  public function actionLogout()
  {
    Yii::app()->user->logout();
    $this->redirect(Yii::app()->homeUrl);
  }
}
