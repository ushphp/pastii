<?php

class UserController extends CController
{
  const PAGE_SIZE=10;

  /**
   * @var string specifies the default action to be 'list'.
   */
  public $defaultAction='list';

  /**
   * @var CActiveRecord the currently loaded data model instance.
   */
  private $_model;

  /**
   * @return array action filters
   */
  public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
        'actions'=>array('create', 'update', 'delete','admin', 'list', 'show', 'toggle'),
        'users'=>array('@'),
      ),
      array('deny',  // deny all users
        'users'=>array('*'),
      ),
    );
  }

  /**
   * Shows a particular model.
   */
  public function actionShow()
  {
    $this->render('show',array('model'=>$this->loadUser()));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'show' page.
   */
  public function actionUpdate()
  {
    $model = $this -> loadUser(Yii::app() -> user -> id);
    $model -> setScenario('update');
    if(isset($_POST['User']))
    {
      $attrib = $_POST['User'];
      if (isset($attrib['pass']) && $attrib['pass'] == '' && isset($attrib['confirmPass']) && $attrib['confirmPass'] == '') {
        unset($attrib['pass']);
        unset($attrib['confirmPass']);
      }
      $model -> attributes = $attrib;
      if ($model -> validate()) {
        if (isset($_POST['User']['pass']) && $_POST['User']['pass'] != '')
          $model -> pass = User::mycrypt($model -> pass);
        if($model->save(false)) {
          Yii::app()->user->setFlash('success',"User updated!");
          $this->redirect(array('update','id'=>$model->id));
        }
      }
    }
    $this->render('update',array('model'=>$model));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'show' page.
   */
  public function actionCreate()
  {
    $model = new User;
    $model -> setScenario('modify');
    if(isset($_POST['User']))
    {
      $model -> attributes=$_POST['User'];
      if ($model -> validate()) {
        $model -> pass = User::mycrypt($model -> pass);
        $model -> createTime = time();
        $model -> active = true;
        if($model -> save(false)) {
          $identity = new UserIdentity($model -> nick, $model -> pass);
          $identity -> authenticate();
          // analizar de poner rememberMe o no al create/register
          Yii::app() -> user -> login($identity, 0);
          Yii::app()->user->setFlash('success',"User created!");
          $this -> redirect(dirname(Yii::app() -> user -> returnUrl));
        }
      }
    }
    $this -> render('create', array('model' => $model));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'list' page.
   */
  public function actionDelete()
  {
    $this->loadUser($_POST['id'])->delete();
    if ($isAdmin)
      $this->redirect(array('/user/list'));
    else
      $this->redirect(array('/site/logout'));
  }

  /**
   * Lists all models.
   */
  public function actionList()
  {
    $criteria=new CDbCriteria;

    $pages=new CPagination(User::model()->count($criteria));
    $pages->pageSize=self::PAGE_SIZE;
    $pages->applyLimit($criteria);

    $models=User::model()->findAll($criteria);

    $this->render('list',array(
      'models'=>$models,
      'pages'=>$pages,
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    $this->processAdminCommand();

    $criteria=new CDbCriteria;

    $pages=new CPagination(User::model()->count($criteria));
    $pages->pageSize=self::PAGE_SIZE;
    $pages->applyLimit($criteria);

    $sort=new CSort('User');
    $sort->applyOrder($criteria);

    $models=User::model()->findAll($criteria);

    $this->render('admin',array(
      'models'=>$models,
      'pages'=>$pages,
      'sort'=>$sort,
    ));
  }

  public function actionToggle() {
    $model = $this -> loadUser();
    $model -> active = !$model -> active;
    $model -> save(false);
    $this -> redirect(array('/user/admin'));
  }


  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
   */
  public function loadUser($id=null)
  {
    if($this->_model===null)
    {
      if($id!==null || isset($_GET['id']))
        $this->_model=User::model()->findbyPk($id!==null ? $id : $_GET['id']);
      if($this->_model===null)
        throw new CHttpException(404,'The requested page does not exist.');
    }
    return $this->_model;
  }

  /**
   * Executes any command triggered on the admin page.
   */
  protected function processAdminCommand()
  {
    if(isset($_POST['command'], $_POST['id']) && $_POST['command']==='delete')
    {
      $this->loadUser($_POST['id'])->delete();
      // reload the current page to avoid duplicated delete actions
      $this->refresh();
    }
  }
}
