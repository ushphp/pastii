CREATE TABLE User (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  type VARCHAR(255) DEFAULT 'user',
  nick VARCHAR(255),
  pass VARCHAR(255),
  image VARCHAR(255),
  active BOOL,
  createTime INTEGER,
  updateTime INTEGER
)ENGINE=INNODB;

CREATE TABLE Paste (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  type VARCHAR(255) DEFAULT'text',
  nick VARCHAR(255),
  title VARCHAR(255),
  text LONGTEXT,
  until INTEGER,
  privateKey VARCHAR(255),
  createTime INTEGER,
  updateTime INTEGER,
  ip VARCHAR(255),
  pasteId INTEGER
)ENGINE=INNODB;

INSERT INTO User 
(type, nick, pass, image, active, createTime, updateTime)
VALUES
('admin','totoloco','b3fdf8192ef01cf6d5aab9cdd5a7f72765d5b529',NULL,1,NULL,1263318349);
