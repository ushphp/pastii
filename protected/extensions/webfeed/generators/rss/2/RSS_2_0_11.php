<?php
/**
 * RSS_2_0 class file
 *
 * @author MetaYii
 * @version 1.0
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008 MetaYii
 * @license
 *
 * Copyright © 2008 by MetaYii. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * - Neither the name of MetaYii nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * About the RSS 2.0 specification:
 *
 * "RSS 2.0 is offered by the Berkman Center for Internet & Society at Harvard
 * Law School under the terms of the Attribution/Share Alike Creative Commons
 * license. The author of this document is Dave Winer, founder of UserLand
 * software, and fellow at Berkman Center."
 *
 * The Attribution/Share Alike Creative Commons License can be found {here:
 * @link http://creativecommons.org/licenses/by-sa/1.0/}
 */

/**
 * RSS_2_0 implements the RSS 2.0 (Really Simple Sindication) specification as
 * explained {here @see http://cyber.law.harvard.edu/rss/rss.html}
 *
 * @author MetaYii
 * @link http://cyber.law.harvard.edu/rss/rss.html
 */

require_once('RSS_2_0_1.php');

class RSS_2_0_11 extends RSS_2_0_1 implements IFeedGenerator {
   //***************************************************************************
   // Constructor
   //***************************************************************************

   public function __construct($title, $description, $link, $language='')
   {
      parent::__construct($title, $description, $link, $language);
   	 	$this->docs = 'http://www.rssboard.org/rss-2-0-11';
   }
}