<?php

class Paste extends CActiveRecord
{
  // $until is in seconds
  /**
   * The followings are the available columns in table 'Paste':
   * @var integer $id
   * @var string $type
   * @var string $title
   * @var string $text
   * @var string $until
   * @var string $privateKey
   * @var integer $createTime
   * @var integer $updateTime
   * @var integer $pasteId
   * @var string $ip
   */

  /**
   * Returns the static model of the specified AR class.
   * @return CActiveRecord the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'Paste';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    return array(
      array('text','required'),
      array('type','required'),
      array('nick','length','max'=>255),
      array('title','length','max'=>255),
      array('type','length','max'=>255),
      array('until,pasteId','numerical'),
      array('until','default', 'setOnEmpty' => true, 'value' => 604800), // 1 week
      array('privateKey','length','max'=>255),
      array('pasteId', 'required', 'on' => 'reply'),
    );
  }

  protected function beforeValidate() {
    $this -> updateTime = time();
    if (!is_null($this -> until))
      $this -> until *= 3600;
    $this -> text = str_replace("\r", "", $this -> text);
    if ($this -> isNewRecord) {
      $this -> createTime = $this -> updateTime;
      // $guest = Yii::app() -> user -> isGuest;
      $this -> ip = $_SERVER['REMOTE_ADDR'];
      // if ($guest || is_null($this -> pasteId))
      if (is_null($this -> pasteId))
        $this -> pasteId = 0;
      if (is_null($this -> type))
        $this -> type = 'text';
    }
    return true;
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id' => 'Id',
      'type' => 'Type',
      'nick' => 'Nick',
      'text' => 'Text',
      'privateKey' => 'Private Key',
      'createTime' => 'Create Time',
      'updateTime' => 'Update Time',
      'pasteId' => 'Parent Paste',
      'verifyCode'=>'Verification Code',
    );
  }
  
  public function getUntilOptions() {
    return array(
      168 => '1 week',
      0 => 'forever',
      1 => '1 hour',
      3 => '3 hours',
      6 => '6 hours',
      12 => '12 hours',
      24 => '1 day',
      72 => '3 day',
      120 => '5 day',
      360 => '15 days',
      720 => '30 days',
      1440 => '60 days',
      2208 => '3 months',
      2908 => '4 months',
      4392 => '6 months',
      8250 => '9 months',
      8760 => '1 year'
    );
  }
  function getUntilText() {
    $options = $this -> getUntilOptions();
    $text = isset($options[$this -> until])? $options[$this -> type]: '1 week';
  }

  public function getTypeOptions() {
    return array(
      '4cs' => 'GADV 4CS', 
      'abap' => 'ABAP', 
      'actionscript' => 'ActionScript', 
      'actionscript3' => 'ActionScript 3', 
      'ada' => 'Ada', 
      'apache' => 'Apache configuration', 
      'applescript' => 'AppleScript', 
      'apt_sources' => 'Apt sources', 
      'asm' => 'ASM', 
      'asp' => 'ASP', 
      'autohotkey' => 'Autohotkey', 
      'autoit' => 'AutoIt', 
      'avisynth' => 'AviSynth', 
      'awk' => 'awk', 
      'bash' => 'Bash', 
      'basic4gl' => 'Basic4GL', 
      'bf' => 'Brainfuck', 
      'bibtex' => 'BibTeX', 
      'blitzbasic' => 'BlitzBasic', 
      'bnf' => 'bnf', 
      'boo' => 'Boo', 
      'c' => 'C', 
      'c_mac' => 'C (Mac)', 
      'caddcl' => 'CAD DCL', 
      'cadlisp' => 'CAD Lisp', 
      'cfdg' => 'CFDG', 
      'cfm' => 'ColdFusion', 
      'cil' => 'CIL', 
      'cmake' => 'CMake', 
      'cobol' => 'COBOL', 
      'cpp' => 'C++', 
      'cpp-qt' => '__C++ (QT)', 
      'csharp' => 'C#', 
      'css' => 'CSS', 
      'd' => 'D', 
      'dcs' => 'DCS', 
      'delphi' => 'Delphi', 
      'diff' => 'Diff', 
      'div' => 'DIV', 
      'dos' => 'DOS', 
      'dot' => 'dot', 
      'eiffel' => 'Eiffel', 
      'email' => 'eMail (mbox)', 
      'erlang' => 'Erlang', 
      'fo' => 'FO (abas-ERP)', 
      'fortran' => 'Fortran', 
      'freebasic' => 'FreeBasic', 
      'gdb' => 'GDB', 
      'genero' => 'genero', 
      'gettext' => 'GNU Gettext', 
      'glsl' => 'glSlang', 
      'gml' => 'GML', 
      'gnuplot' => 'Gnuplot', 
      'groovy' => 'Groovy', 
      'haskell' => 'Haskell', 
      'hq9plus' => 'HQ9+', 
      'html4strict' => 'HTML', 
      'idl' => 'Uno Idl', 
      'ini' => 'INI', 
      'inno' => 'Inno', 
      'intercal' => 'INTERCAL', 
      'io' => 'Io', 
      'java' => 'Java', 
      'java5' => 'Java(TM) 2 Platform Standard Edition 5.0', 
      'javascript' => 'Javascript', 
      'jquery' => 'jQuery', 
      'kixtart' => 'KiXtart', 
      'klonec' => 'KLone C', 
      'klonecpp' => 'KLone C++', 
      'latex' => 'LaTeX', 
      'lisp' => 'Lisp', 
      'locobasic' => 'Locomotive Basic', 
      'lolcode' => 'LOLcode', 
      'lotusformulas' => 'Lotus Notes @Formulas', 
      'lotusscript' => 'LotusScript', 
      'lscript' => 'LScript', 
      'lsl2' => 'LSL2', 
      'lua' => 'Lua', 
      'm68k' => 'Motorola 68000 Assembler', 
      'make' => 'GNU make', 
      'matlab' => 'Matlab M', 
      'mirc' => 'mIRC Scripting', 
      'modula3' => 'Modula-3', 
      'mpasm' => 'Microchip Assembler', 
      'mxml' => 'MXML', 
      'mysql' => 'MySQL', 
      'nsis' => 'NSIS', 
      'oberon2' => 'Oberon-2', 
      'objc' => 'Objective-C', 
      'ocaml' => 'OCaml', 
      'ocaml-brief' => '__OCaml (brief)', 
      'oobas' => 'OpenOffice.org Basic', 
      'oracle11' => 'Oracle 11 SQL', 
      'oracle8' => 'Oracle 8 SQL', 
      'pascal' => 'Pascal', 
      'per' => 'per', 
      'perl' => 'Perl', 
      'php' => 'PHP', 
      'php-brief' => '__PHP (brief)', 
      'pic16' => 'PIC16', 
      'pixelbender' => 'Pixel Bender 1.0', 
      'plsql' => 'PL/SQL', 
      'povray' => 'POVRAY', 
      'powerbuilder' => 'PowerBuilder', 
      'powershell' => 'PowerShell', 
      'progress' => 'Progress', 
      'prolog' => 'Prolog', 
      'properties' => 'PROPERTIES', 
      'providex' => 'ProvideX', 
      'purebasic' => 'PureBasic', 
      'python' => 'Python', 
      'qbasic' => 'QBasic/QuickBASIC', 
      'qbasic' => 'QBasic/QuickBASIC', 
      'rails' => 'Rails', 
      'rebol' => 'REBOL', 
      'reg' => 'Microsoft Registry', 
      'robots' => 'robots.txt', 
      'rsplus' => 'r', 
      'ruby' => 'Ruby', 
      'sas' => 'SAS', 
      'scala' => 'Scala', 
      'scheme' => 'Scheme', 
      'scilab' => 'SciLab', 
      'sdlbasic' => 'sdlBasic', 
      'smalltalk' => 'Smalltalk', 
      'smarty' => 'Smarty', 
      'sql' => 'SQL', 
      'tcl' => 'TCL', 
      'teraterm' => 'Tera Term Macro', 
      'text' => 'Text', 
      'thinbasic' => 'thinBasic', 
      'tsql' => 'T-SQL', 
      'typoscript' => 'TypoScript', 
      'vb' => 'Visual Basic', 
      'vbnet' => 'vb.net', 
      'verilog' => 'Verilog', 
      'vhdl' => 'VHDL', 
      'vim' => 'Vim Script', 
      'visualfoxpro' => 'Visual Fox Pro', 
      'visualprolog' => 'Visual Prolog', 
      'whitespace' => 'Whitespace', 
      'whois' => 'Whois (RPSL format)', 
      'winbatch' => 'Winbatch', 
      'xml' => 'XML', 
      'xorg_conf' => 'Xorg configuration', 
      'xpp' => 'X++', 
      'z80' => 'ZiLOG Z80 Assembler', 
    );
  }
  function getTypeText() {
    $options = $this -> getTypeOptions();
    $text = isset($options[$this -> type])? $options[$this -> type]: 'Text';
  }

  public function pastesOnHome($count) {
    $criteria = new CDbCriteria;
    $criteria -> order = 'createTime DESC';
    $criteria->limit = $count;
    $criteria -> condition = '(privateKey = \'\' OR privateKey IS NULL)';
    $models = Paste::model() -> findAll($criteria);
    return $models;
  }
}
