<?php

class User extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'User':
	 * @var integer $id
	 * @var string $type
	 * @var string $pass
	 * @var string $nick
	 * @var string $hash
	 * @var boolean $active
	 * @var integer $createTime
	 * @var integer $updateTime
	 */
  public $confirmPass;
  public $rememberMe;
  public $verifyCode;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'User';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
      array('nick','required'),
      array('nick','unique'),
			array('type','length','max'=>255),
			array('nick','length','max'=>255),
			array('pass','length','max'=>255, 'min' => 6),
			array('active, createTime, updateTime', 'numerical', 'integerOnly'=>true),
			array('pass','compare','compareAttribute' => 'confirmPass', 'on' => 'modify'),
			array('pass, confirmPass','required', 'on' => 'modify'),
      array('verifyCode', 'captcha', 'captchaAction' => 'site/captcha', 'allowEmpty' => !extension_loaded('gd') || !Yii::app() -> user -> isGuest),
		);
	}

  public function safeAttributes()
  {
    return array(
      parent::safeAttributes(),
      'modify' => 'active, nick, pass, confirmPass, verifyCode',
    );
  }

  protected function afterValidate() {
    $this -> updateTime = time();
  }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'type' => 'Type',
			'pass' => 'Pass',
			'nick' => 'Nick',
			'hash' => 'Hash',
			'active' => 'Active',
			'createTime' => 'Create Time',
			'updateTime' => 'Update Time',
			'rememberMe'=>'Remember me next time',
      'verifyCode'=>'Verification Code',
		);
	}

  public static function mycrypt($str) {
    return sha1(Yii::app() -> params['salt']. $str);
  }
}
