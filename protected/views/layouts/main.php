<?php
Yii::app() -> clientScript -> registerCoreScript('jquery');
$path = CHtml::asset(Yii::app() -> basePath . '/js/main.js');
Yii::app() -> clientScript -> registerScriptFile($path, CClientScript::POS_END);
Yii::app() -> clientScript -> registerLinkTag('alternate', 'application/rss+xml', $this -> createUrl('paste/feed'));
$home = Yii::app()->homeUrl;
$BASE = Yii::app()->baseUrl;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<title><?php echo $this->pageTitle; ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $BASE; ?>/css/style.css" media="screen, projection" />
</head>

<body>
<input type="hidden" id="BASE_URL" value="<?php echo Yii::app()->request->baseUrl; ?>" />
<?php
$search = isset($_GET['search'])? htmlspecialchars($_GET['search']): '';
$this->widget('bootstrap.widgets.BootNavbar', array(
    'fixed'=>false,
    'brand'=>Yii::app()->name,
    'brandUrl'=>$home,
    'collapse'=>true, // requires bootstrap-responsive.css
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.BootMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/paste/create')),
                array('label'=>'Pastes', 'url'=>array('/paste/list')),
                array('label'=>'About', 'url'=>array('/site/index')),
            ),
        ),
        '<form class="navbar-search pull-left" action="'.$BASE.'/paste/search"><input type="text" name="search" value="'.$search.'" class="search-query span2" placeholder="Search"></form>',
        array(
            'class'=>'bootstrap.widgets.BootMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=>Yii::app() -> user -> name, 'url'=>array('user/update'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'*Pastes', 'url'=>array('/paste/admin'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'*Users', 'url'=>array('/user/admin'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Logout', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
            ),
        ),
    ),
));
?>
<div class="container" id="page">
  <?php echo $content; ?>
  <hr />
  <footer>
    <div id="logo">
      <a href="<?php echo Yii::app()->request->baseUrl; ?>/"><?php echo CHtml::encode(Yii::app()->name); ?></a>
    </div>
    Copyright &copy; 2009-<?php echo date('Y');?> by
    <a href="http://ushcompu.com.ar/">ushcompu</a>
    All Rights Reserved.<br/>
    All content is under
    <a href="http://creativecommons.org/licenses/by/3.0/">creative commons attribution 3.0</a> <br/>
    Powered by <a href="http://www.yiiframework.com/">Yii Framework</a>
  </footer>

</div><!-- page -->
<?php
Yii::app() -> clientScript -> renderCoreScripts('jquery');
?>
</body>

</html>
