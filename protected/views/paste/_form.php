<?php
echo CHtml::beginForm();
?>
  <div class="row">
    <p>
      <span class="label label-success">
      Fields with <span class="required">*</span> are required.
      </span>
    </p>
  
<?php
  echo CHtml::errorSummary($model, NULL, NULL, array('class'=>'label label-important'));
  $i = 1;
  
  echo CHtml::activeLabelEx($model,'type');
  echo CHtml::activeDropDownList($model, 'type', Paste::model() -> getTypeOptions(), array('tabindex'=>$i++, 'class'=>'span12'));
  echo CHtml::activeLabelEx($model,'text');
  echo CHtml::activeTextArea($model,'text',array('class'=>'span12', 'rows'=>6, 'cols'=>50, 'tabindex'=>$i++));
?>
  
  <h3>Extra fields</h3>
  </div>
  <div class="row">
    <div class="span9">
<?php
      echo CHtml::activeLabelEx($model,'title');
      echo CHtml::activeTextField($model,'title',array('class'=>'span9', 'size'=>60,'maxlength'=>255, 'tabindex'=>$i++));
      echo CHtml::activeLabelEx($model,'nick');
      echo CHtml::activeTextField($model,'nick',array('value' => $nick, 'size'=>60,'maxlength'=>255, 'tabindex'=>$i++));
      echo CHtml::activeLabelEx($model,'privateKey');
      echo CHtml::activeTextField($model,'privateKey',array('size'=>60,'maxlength'=>255, 'tabindex'=>$i++));
      echo CHtml::activeLabelEx($model,'until');
      echo CHtml::activeDropDownList($model, 'until', Paste::model() -> getUntilOptions(), array('tabindex'=>$i++));

      if ($this -> action -> id == 'reply')
        echo CHtml::activeHiddenField($model, 'pasteId', array('value' => $model -> id));
?>
      <div>
        <button type="submit" class="btn btn-primary btn-large" tabindex="<?php echo $i++;?>">Submit &raquo;</button>
      </div>
    </div><!-- class="span9" -->
    <div class="span3">
      
      <div id="ajax-pastes">
        <h3><?php echo CHtml::link('More Pastes',array('list')); ?></h3>
<?php
      echo $this->renderPartial('ajax', array(
        'models'=>$models,
      ));
?>
      </div>
      
    </div><!-- class="span3" -->
  </div><!-- class="row" -->
<?php
echo CHtml::endForm();
?>
