<?php
$urlBase = $pages -> createPageUrl($this, 0);
?>
<h2><?php echo CHtml::link('Managing Paste',array('admin')); ?></h2>

<div class="actionBar">
[<?php echo CHtml::link('Paste List',array('list')); ?>]
[<?php echo CHtml::link('New Paste',array('create')); ?>]
</div>

<?php echo CHtml::beginForm(); ?>
  <input type="submit" value="Delete all selected" />
<table class="dataGrid">
  <thead>
  <tr>
    <th>
      <input type="checkbox" id="2delMarkAll" />
      <?php echo $sort->link('id'); ?>
    </th>
    <th><?php echo $sort->link('type'); ?></th>
    <th><?php echo $sort->link('title'); ?></th>
    <th><?php echo $sort->link('privateKey'); ?></th>
    <th><?php echo $sort->link('createTime'); ?></th>
    <th><?php echo $sort->link('updateTime'); ?></th>
    <th><?php echo $sort->link('nick'); ?></th>
    <th><?php echo $sort->link('ip'); ?></th>
    <th><?php echo $sort->link('pasteId'); ?></th>
  <th>Actions</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($models as $n=>$model): ?>
  <tr class="<?php echo $n%2?'even':'odd';?>">
    <td>
      <input type="checkbox" name="2del[]" class="2del" value="<?php echo $model->id;?>" />
      <?php echo CHtml::link($model->id,array('show','id'=>$model->id)); ?>
    </td>
    <td><?php echo CHtml::encode($model->type); ?></td>
    <td><?php echo CHtml::encode($model->title); ?></td>
    <td><?php echo CHtml::encode($model->privateKey); ?></td>
    <td><?php echo date('Y-m-d H:i', $model->createTime); ?></td>
    <td><?php echo date('Y-m-d H:i', $model->updateTime); ?></td>
    <td>
      <?php
        $nick = CHtml::encode($model -> nick);
        if ($nick != '')
          echo CHtml::link($nick, array('list', 'author' => urlencode($model -> nick)));
      ?>
    </td>
    <td>
      <?php echo CHtml::encode($model -> ip); ?>
      <a href="<?php echo $urlBase . '/ip/' . $model -> ip;?>">pastes</a>
    </td>
    <td><?php echo CHtml::encode($model->pasteId); ?></td>
    <td>
      <?php echo CHtml::link('Update',array('update','id'=>$model->id)); ?>
      <?php echo CHtml::linkButton('Delete',array(
          'submit'=>'',
          'params'=>array('command'=>'delete','id'=>$model->id),
          'confirm'=>"Are you sure to delete #{$model->id}?")); ?>
  </td>
  </tr>
<?php endforeach; ?>
  </tbody>
</table>
  <input type="submit" value="Delete all selected" />
<?php echo CHtml::endForm(); ?>
<a href="<?php echo $urlBase . '/pageSize/5';?>">5</a>
<a href="<?php echo $urlBase . '/pageSize/10';?>">10</a>
<a href="<?php echo $urlBase . '/pageSize/20';?>">20</a>
<a href="<?php echo $urlBase . '/pageSize/50';?>">50</a>
<a href="<?php echo $urlBase . '/pageSize/100';?>">100</a>
<a href="<?php echo $urlBase . '/pageSize/100';?>">100</a>
<a href="<?php echo $urlBase . '/pageSize/0';?>">ALL</a>

<?php $this->widget('CLinkPager',array('pages'=>$pages)); ?>
