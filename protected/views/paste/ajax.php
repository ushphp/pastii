<?php
// EX ajax view/partial
foreach($models as $n => $model) {
  $id = $model -> id;
  $link = $this -> createAbsoluteUrl('show', array('id' => $id));
  $pasteId = $model -> pasteId;
  $type = CHtml::encode($model -> type);
  $title = CHtml::encode($model -> title);
  if ($title == '')
    $title = '[view]';
  $nick = CHtml::encode($model -> nick);
  $text = CHtml::encode(mb_substr($model -> text, 0, 80, 'utf-8'));
  if (mb_strlen($model -> text, 'utf-8') > 80)
    $text .= '[...]';
  ?>
    <div class="paste">
      <strong><?php echo CHtml::link($title, array('show', 'id' => $id)); ?></strong>
      <?php echo CHtml::link('[raw]', array('raw', 'id' => $id)); ?>
      <?php echo CHtml::link('[reply]', array('reply', 'id' => $id)); ?>
      <em>type</em>: <?php echo CHtml::link($type, array('list', 'type' => $type));?>
      <?php
      if ($nick != '')
        echo '<em>author</em>: ' . CHtml::link($nick, array('list', 'author' => urlencode($model -> nick)));
      ?>
      <div><?php echo $text;?></div>
    </div>
  <?php
}
