<?php
$feed = new EWebFeed('RSS_2_0_11', array('title' => 'Pastes', 'description' => 'Pastii Pastes', 'link' => $this -> createAbsoluteUrl('/') . '/'));
$feed -> charset = 'UTF-8';
$feed -> pubDate = time();
$feed -> ttl = 300;

foreach($models as $n => $model) {
  $id = $model -> id;
  $link = $this -> createAbsoluteUrl('show', array('id' => $id));
  $pasteId = $model -> pasteId;
  $type = CHtml::encode($model -> type);
  $title = CHtml::encode($model -> title);
  if ($title == '')
    $title = '[paste]';
  $nick = CHtml::encode($model -> nick);
  $text = CHtml::encode($model -> text);

  $item = $feed -> addItem($title, $link, $text);
  $item -> guid = $link;
  $item -> guidIsPermaLink = true;
  $item -> author = $nick;
  $item -> pubDate = (int)$model -> createTime;
}
$feed -> dumpXML();
