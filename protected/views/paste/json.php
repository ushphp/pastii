<?php
$cb = isset($_GET['cb'])? $_GET['cb']: '';
if ($cb != '')
  echo $cb . '(';
echo '[';
foreach($models as $n => $model) {
  $id = $model -> id;
  $link = $this -> createAbsoluteUrl('show', array('id' => $id));
  $pasteId = $model -> pasteId;
  $type = CHtml::encode($model -> type);
  $title = CHtml::encode($model -> title);
  $nick = CHtml::encode($model -> nick);
  $text = CHtml::encode(str_replace("\n", "\\n", mb_substr($model -> text, 0, 80, 'utf-8')));
  if (mb_strlen($model -> text, 'utf-8') > 80)
    $text .= '[...]';
  if ($n > 0)
    echo ',';
  echo "{'id':$id,'title':'$title','nick':'$nick','type':'$type','pasteId':'$pasteId','text':'$text','link':'$link'}";
}
echo ']';
if ($cb != '')
  echo ');';
