<div class="hero-unit">
<h2>
<?php echo CHtml::link('Paste List', array('list'));?>
<div class="yiiForm">
  <form action="<?php echo yii::app() -> urlManager -> baseUrl;?>/paste/search">
    <input type="text" name="search" id="Search_text" />
    <input type="submit" value="Search" />
  </form>
</div>
</h2>

<?php $this->widget('CLinkPager',array('pages'=>$pages)); ?>

<?php
foreach($models as $n=>$model) {
  $id = $model -> id;
  $type = CHtml::encode($model -> type);
  $title = CHtml::encode($model -> title);
  $nick = CHtml::encode($model -> nick);
  if ($title == '')
    $title = '[view]';
?>
  <div class="item">
  <strong><?php echo CHtml::link($title, array('show', 'id' => $id)); ?></strong>
  | <?php echo CHtml::link('[raw]', array('raw', 'id' => $id)); ?>
  | <?php echo CHtml::link('[reply]', array('reply', 'id' => $id)); ?>
  <?php
  if (!Yii::app() -> user -> isGuest) {
  ?>
    | <?php echo CHtml::link('[update]', array('update', 'id' => $id)); ?>
  <?php
  }
  if ($model -> pasteId > 0) {
  ?>
    <br /><?php echo CHtml::link('[parent]', array('show', 'id' => $model -> pasteId)); ?>
  <?php 
  }
  ?>
  <br />
  <em>type</em>: <?php echo CHtml::link($type, array('list', 'type' => $type));?>
  <?php 
  if ($nick != '')
    echo ' | <em>author</em>: ' . CHtml::link($nick, array('list', 'author' => urlencode($model -> nick)));
  ?>
  <?php
  if ($model -> title != '')
    echo '<br/> ' . CHtml::encode($model->getAttributeLabel('title')) . ': ' . CHtml::encode($model->title);
  ?>
  <br/>
  <?php 
  echo CHtml::encode(mb_substr($model->text, 0, 80, 'utf-8'));
  if (mb_strlen($model -> text, 'utf-8') > 80)
    echo '[...]';
  ?>

  <br/>
  <?php echo date('Y-m-d H:i O', $model->createTime); ?>
  
  </div>
<?php 
}
?>
<br/>
<?php $this->widget('CLinkPager',array('pages'=>$pages)); ?>
<div class="actionBar">
[<?php echo CHtml::link('New Paste',array('create')); ?>]
</div>
</div>
