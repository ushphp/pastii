<?php 
echo $this -> renderPartial('show', array(
  'model'  => $model,
));
echo $this -> renderPartial('_form', array(
  'models' => $models,
  'model'  => $model,
  'nick'   => $nick,
  'update' => false,
));
?>
