<?php
$geshi = new GeSHi($model -> text, $model -> type);
$geshi->line_nth_row = 2;
$geshi->line_numbers = GESHI_FANCY_LINE_NUMBERS;
$geshi->use_classes = true;
$geshi->allow_multiline_span = true;
$geshi->header_type = GESHI_HEADER_DIV;

$type = CHtml::encode($model -> type);
$nick = CHtml::encode($model -> nick);
$title = CHtml::encode($model -> title);
?>
<style type="text/css">
<?php echo $geshi -> get_stylesheet(); ?>
</style>
<div class="geshi"><?php echo $geshi -> parse_code(); ?></div>
<?php
?>
<h4>
<?php
echo CHtml::link($type, array('list', 'type' => $type));
if ($nick != '')
  echo ' | author: ' . CHtml::link($nick, array('list', 'author' => urlencode($model -> nick)));
if ($title != '')
  echo ' | <em>' . $title . '</em>';
?>
  | <em>@<?php echo date('Y-m-d H:i O', $model->createTime);?></em>
</h4>

<div class="actionBar">
[<?php echo CHtml::link('Raw',array('raw','id'=>$model->id)); ?>]
[<?php echo CHtml::link('Reply',array('reply','id'=>$model->id)); ?>]
[<?php echo CHtml::link('Paste List',array('list')); ?>]
[<?php echo CHtml::link('New Paste',array('create')); ?>]
<?php
if (!Yii::app() -> user -> isGuest) {
?>
  [<?php echo CHtml::link('[update]', array('update', 'id' => $model -> id)); ?>]
<?php
}
?>
<hr />
<?php
if ($model -> pasteId) {
?>
  <h4>In reply to:</h4>
<?php 
  $this->renderPartial('ajax', array('models'=>array($parent)));
}
if (isset($childs[0])) {
?>
  <h4>Replies:</h4>
<?php
  $this->renderPartial('ajax', array('models'=>$childs));
}
?>
</div>

