<h2>Update Paste <?php echo $model->id; ?></h2>

<div class="actionBar">
[<?php echo CHtml::link('View Paste',array('show', 'id' => $model -> id)); ?>]
[<?php echo CHtml::link('Paste List',array('list')); ?>]
[<?php echo CHtml::link('New Paste',array('create')); ?>]
</div>

<?php echo $this->renderPartial('_form', array(
	'model'=>$model,
	'update'=>true,
	'nick'=>$nick,
)); ?>
