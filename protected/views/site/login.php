<?php $this->pageTitle=Yii::app()->name . ' - Login'; ?>

<h1>Login</h1>

<div class="yiiForm">
<?php echo CHtml::beginForm(); ?>

<?php echo CHtml::errorSummary($form); ?>

<div class="simple">
<?php echo CHtml::activeLabel($form,'nick'); ?>
<?php echo CHtml::activeTextField($form,'nick') ?>
</div>

<div class="simple">
<?php echo CHtml::activeLabel($form,'pass'); ?>
<?php echo CHtml::activePasswordField($form,'pass') ?>
</div>

<div class="action">
<?php echo CHtml::activeCheckBox($form,'rememberMe'); ?>
<?php echo CHtml::activeLabel($form,'rememberMe'); ?>
<br/>
<?php echo CHtml::submitButton('Login'); ?>
</div>

</div><!-- yiiForm -->
