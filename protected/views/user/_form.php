<div class="yiiForm">

<p>
Fields with <span class="required">*</span> are required.
</p>

<?php echo CHtml::beginForm(); ?>

<?php echo CHtml::errorSummary($model); ?>

<div class="simple">
<?php echo CHtml::activeLabelEx($model,'nick'); ?>
<?php echo CHtml::activeTextField($model,'nick',array('size'=>60,'maxlength'=>255)); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'pass'); ?>
<?php echo CHtml::activePasswordField($model,'pass',array('size'=>60,'maxlength'=>255, 'value' => '')); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'confirmPass'); ?>
<?php echo CHtml::activePasswordField($model,'confirmPass',array('size'=>60,'maxlength'=>255, 'value' => '')); ?>
</div>

<?php
if (extension_loaded('gd') && Yii::app() -> user -> isGuest) {
?>
  <div class="simple">
    <?php echo CHtml::activeLabel($model, 'verifyCode'); ?>
    <div>
    <?php $this -> widget('CCaptcha', array('captchaAction' => 'site/captcha')); ?>
    <?php echo CHtml::activeTextField($model, 'verifyCode'); ?>
    </div>
    <p class="hint">Please enter the letters as they are shown in the image above.
    <br/>Letters are not case-sensitive.</p>
  </div>
<?php
}
?>
<div class="simple">
<?php echo CHtml::activeLabel($model, 'active'); ?>
<?php echo CHtml::activeCheckBox($model,'active'); ?>
</div>

<div class="action">
<?php echo CHtml::submitButton($update ? 'Save' : 'Create'); ?>
</div>

<?php echo CHtml::endForm(); ?>

</div><!-- yiiForm -->
