<h2>Managing User</h2>

<div class="actionBar">
[<?php echo CHtml::link('User List',array('list')); ?>]
[<?php echo CHtml::link('New User',array('create')); ?>]
</div>

<table class="dataGrid">
  <thead>
  <tr>
    <th><?php echo $sort->link('id'); ?></th>
    <th><?php echo $sort->link('type'); ?></th>
    <th><?php echo $sort->link('nick'); ?></th>
    <th><?php echo $sort->link('active'); ?></th>
    <th><?php echo $sort->link('createTime'); ?></th>
    <th><?php echo $sort->link('updateTime'); ?></th>
  <th>Actions</th>
  </tr>
  </thead>
  <tbody>
<?php foreach($models as $n=>$model): ?>
  <tr class="<?php echo $n%2?'even':'odd';?>">
    <td><?php echo CHtml::link($model->id,array('show','id'=>$model->id)); ?></td>
    <td><?php echo CHtml::encode($model->type); ?></td>
    <td><?php echo CHtml::encode($model->nick); ?></td>
    <td>
      <?php echo $model -> active? 'active': 'inactive'; ?>
      <?php echo CHtml::link('[toggle]', array('toggle','id' => $model -> id)); ?>
    </td>
    <td><?php echo date('Y-m-d H:i', $model->createTime); ?></td>
    <td><?php echo date('Y-m-d H:i', $model->updateTime); ?></td>
    <td>
      <?php echo CHtml::link('Update',array('update','id'=>$model->id)); ?>
      <?php echo CHtml::linkButton('Delete',array(
          'submit'=>'',
          'params'=>array('command'=>'delete','id'=>$model->id),
          'confirm'=>"Are you sure to delete #{$model->id}?")); ?>
  </td>
  </tr>
<?php endforeach; ?>
  </tbody>
</table>
<br/>
<?php $this->widget('CLinkPager',array('pages'=>$pages)); ?>
