<?php $this->pageTitle=Yii::app()->name . ' - Register'; ?>

<h1>Register</h1>

<div class="yiiForm">
<?php echo CHtml::beginForm(); ?>

<?php echo CHtml::errorSummary($model); ?>

<div class="simple">
<?php echo CHtml::activeLabel($model,'nick'); ?>
<?php echo CHtml::activeTextField($model,'nick') ?>
</div>

<div class="simple">
<?php echo CHtml::activeLabel($model,'pass'); ?>
<?php echo CHtml::activePasswordField($model,'pass') ?>
</div>

<div class="simple">
<?php echo CHtml::activeLabel($model,'confirmPass'); ?>
<?php echo CHtml::activePasswordField($model,'confirmPass') ?>
</div>

<div class="action">
<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
<?php echo CHtml::activeLabel($model,'rememberMe'); ?>
<br/>
<?php echo CHtml::submitButton('Login'); ?>
</div>

<?php echo CHtml::endForm(); ?>

</div><!-- yiiForm -->
