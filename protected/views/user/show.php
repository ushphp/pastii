<h2>View User <?php echo $model->id; ?></h2>

<div class="actionBar">
[<?php echo CHtml::link('User List',array('list')); ?>]
[<?php echo CHtml::link('New User',array('create')); ?>]
[<?php echo CHtml::link('Update User',array('update','id'=>$model->id)); ?>]
[<?php echo CHtml::linkButton('Delete User',array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure?')); ?>
]
[<?php echo CHtml::link('Manage User',array('admin')); ?>]
</div>

<table class="dataGrid">
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('nick')); ?>
</th>
    <td><?php echo CHtml::encode($model->nick); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('createTime')); ?>
</th>
    <td><?php echo date('Y-m-d H:i', $model->createTime); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('active')); ?>
</th>
    <td><?php echo CHtml::encode($model->active); ?>
</td>
</tr>
<tr>
	<th class="label">pastes
</th>
    <td><?php echo CHtml::link('pastes', array('/paste/list/author/' . $model -> nick)); ?>
</td>
</tr>
</table>
