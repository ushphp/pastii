<h2>Update User <?php echo $model -> id; ?></h2>

<?php echo $this -> renderPartial('_form', array(
  'model' => $model,
  'update' => true,
)); ?>
[<?php echo CHtml::linkButton('Delete account',array(
    'submit' => array('/user/delete'),
    'params' => array('command'=>'delete','id' => $model -> id),
    'confirm' => "Are you sure to delete your account?")); ?>]
