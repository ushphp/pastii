<html>
  <style type="text/css">
* {
  font-family: Arial, Helvetica, sans-serif;
}
  </style>
<body>
<div>
  <h2>Requirements</h2>
  <ul>
    <li>
      Working on <a target="_blank" href="http://httpd.apache.org/">Apache</a>
      with <a target="_blank" href="http://httpd.apache.org/docs/1.3/mod/mod_rewrite.html">mod_rewrite</a>
    </li>
    <li>
      <a target="_blank" href="http://php.net">Php</a>
    </li>
    <li>
      <a target="_blank" href="http://php.net/manual/en/book.pdo.php">PDO</a>
    </li>
    <ul>
      <li>
        <a target="_blank" href="http://php.net/manual/en/ref.pdo-sqlite.php">PDO for sqlite</a> OR
      </li>
      <li>
        <a target="_blank" href="http://php.net/manual/en/ref.pdo-mysql.php">PDO for mysql</a> OR
      </li>
      <li>
        <a target="_blank" href="http://php.net/manual/en/ref.pdo-pgsql.php">PDO for postgresql</a>
      </li>
    </ul>
  </ul>
</div>
<?php
if (extension_loaded('pdo_sqlite')) {
?>
  <div>
    <em>OK</em>: PDO for sqlite founded
  </div>
<?php
}
if (extension_loaded('pdo_mysql')) {
?>
  <div>
    <em>OK</em>: PDO for mysql founded
  </div>
<?php
}
if (extension_loaded('pdo_pgsql')) {
?>
  <div>
    <em>OK</em>: PDO for postgresql founded
  </div>
<?php
}
?>
<h2>After install delete this dir!!!</h2>
</body>
</html>
